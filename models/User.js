const mongoose = require('mongoose');
const { Schema } = mongoose;
// const Schema = mongoose.Schema; *SAME
const userSchema = new Schema({
    googleID: String,
    name: String,
    email: String,
    photo: String,
    credits: { type: Number, default: 3 },
});

mongoose.model('users', userSchema);