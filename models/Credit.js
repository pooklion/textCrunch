const mongoose = require('mongoose');
const { Schema } = mongoose;
const creditSchema =  new Schema({
    dollar: String,
    dateRefill: Date,
    _user: { type: Schema.Types.ObjectId, ref: 'User '}
});
mongoose.model('credit', creditSchema);