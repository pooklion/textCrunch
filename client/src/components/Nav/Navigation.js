import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    Button,
    Container,
    Grid,
    Menu
  } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import Logo from '../Logo/Logo'

class Navigation extends Component {
    render() {
        return (
            <React.Fragment>
                <Grid container style={{ margin: '0 auto' }}>
                    <Logo/>
                    <Menu.Item as={Link} to='/' onClick={this.props.handleClick} >Home</Menu.Item>
                    <Menu.Item as={Link} to='/text' onClick={this.props.handleClick}>Text</Menu.Item>
                    <Menu.Item as={Link} to='/credit' onClick={this.props.handleClick}>Credit</Menu.Item>
                    <Menu.Item as={Link} to='/about' onClick={this.props.handleClick}>About</Menu.Item>
                    <Menu.Item position='right'>
                    {this.props.isAuth
                    ? <a href='/api/logout'><Button inverted>Log out</Button></a> 
                    : <a href='/auth/google'><Button inverted>Log in with Google</Button></a>}
                    </Menu.Item>
                </Grid>
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        authData: state.auth,
        isAuth: (state.auth !== null) && (state.auth !== false)
    }
}

export default connect(mapStateToProps)(Navigation);