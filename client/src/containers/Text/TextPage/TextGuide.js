import React from 'react';
import { Link } from 'react-router-dom';
import {
    Button,
    Header
  } from 'semantic-ui-react';
  
const TextGuide = () => (
    <div>
        <Header as='h3' style={{ fontSize: '2em' }}>Welcome to Text Crunch</Header>
        <p style={{ fontSize: '1.33em' }}>Send text to any U.S. mobile number for just $1</p>
        <Button as={Link} to='/text/new' color='black'>Start Text</Button>
    </div>
)

export default TextGuide;