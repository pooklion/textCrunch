import React, { Component } from 'react';
import * as actions from '../../store/actions';
import { connect } from 'react-redux';
import { Bar } from 'react-chartjs';
import { updateObject } from '../../shared/utility';

class Usage extends Component {
    state = {
        chartData: {
            // labels: ['Jan','Feb', 'Mar', 'Apr','May', 'Jun', 'Jul'],
            labels: ['Jan','Feb', 'Mar', 'Apr','May', 'Jun', 'Jul'],
            datasets: [
                {
                    label: 'Text',
                    // data:[12,20,35,7,24,32,3],
                    
                    data: [12,20,35,7,24,32,3],
                    fillColor: ["#FF5992","#4CDEF5", "#841983", "#A4D555", "#FF5992","#A4D555","#4CDEF5" ],
                    // strokeColor: "rgba(220,220,220,0.8)", 
                    highlightFill: "rgba(220,220,220,0.75)",
                    highlightStroke: "rgba(220,220,220,1)"
                }
            ],
        }
    }

    chartList = () => {
        let labels = [];
        let data = [];
        this.props.chart && this.props.chart.text_monthly.map((text) => {
            labels.push(text._id)
            data.push(text.users)
        })
        let datasets = this.state.chartData.datasets
        datasets[0].data = data
        this.setState({chartData:{
            ...this.state.chartData,
            labels,
            datasets
        }})
        console.log("state",this.state)
    } 

    componentWillMount() {
        this.props.onFetchTextChart();
    }

    // componentDidUpdate(prevProps, prevState) {
    //     let labels = prevState.chartData.labels
    //     console.log(prevState.chartData)
    //     if(labels && labels.length) {

    //     }
    //     else {
    //         this.chartList();
    //     }
       
    // }
    
    render() { 
        return (
            <div className='chart'>
            <Bar
                data={this.state.chartData}
                option={{
                    maintainAspecRatio: false
                }}/>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        chart: state.text.chart
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchTextChart: () => dispatch(actions.textChart())
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Usage);