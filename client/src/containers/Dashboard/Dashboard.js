import React, { Component } from 'react';
import * as actions from '../../store/actions';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import {
    Button,
    Grid,
    Icon,
    Image,
    Segment,
    Card,
    Header
  } from 'semantic-ui-react';
import Usage from './Usage';
import Credit from '../../components/Credit/CreditActivity';
import Text from '../../components/Text/TextActivity';


class Dashboard extends Component {
    state = {
        tooltipOpen: false
    };

    componentDidMount() {
        this.props.onFetchText({ limit: 3 });
        this.props.onFetchCredit({ limit: 3 });
        this.props.onFetchTextTotal();
    }


    render() {
        const { auth } = this.props;
        return (
            <Segment style={{ padding: '2em 0em' }} vertical>
                <Grid container stackable divided='vertically' verticalAlign='top' textAlign='center'>
                  <Grid.Row columns={3} stretched>
                    <Grid.Column>
                      <Card fluid className='card4 card' style={{ padding: '4em 0em' }} vertical='true'>
                        <Card.Content>
                            <Segment basic>
                                <Image src={auth && auth.photo} size='tiny' circular />
                            </Segment>
                        <Card.Header>{auth && auth.name}</Card.Header>
                        <Card.Description><Card.Meta>Joined in 2016</Card.Meta></Card.Description>
                        </Card.Content>
                        <Segment basic>
                            <a href='/api/logout'><Button>Log out</Button></a>
                        </Segment>
                        <Card.Content extra>
                            By <Icon name='google plus' size='big'/>
                        </Card.Content>
                      </Card>
                    </Grid.Column>

                    <Grid.Column>
                      <Card fluid className='card2 card' style={{ padding: '4em 0em' }} vertical='true'>
                        <Card.Content>
                            <Segment basic>
                                <Header>CREDIT BALANCE</Header>
                            </Segment>
                            <Segment basic>
                                <h1>{auth && auth.credits}</h1>
                            </Segment>
                        </Card.Content>
                        <Segment basic>
                            <Link to={"/credit"}><Button>Add Credit</Button></Link>
                        </Segment>
                        <Card.Content extra>
                            By <Icon name='cc stripe' size='big'/>
                        </Card.Content>
                      </Card>
                    </Grid.Column>

                    <Grid.Column>
                      <Card fluid className='card3 card' style={{ padding: '4em 0em' }} vertical='true'>
                        <Card.Content>
                            <Segment basic>
                                <Header>SENT TOTAL</Header>
                            </Segment>
                            <Segment basic>
                                <h1>{this.props.total}</h1>
                            </Segment>
                        </Card.Content>
                        <Segment basic>
                            <Link to={"/text"}><Button>Send Text</Button></Link>
                        </Segment>
                        <Card.Content extra>
                            By <a href='http://www.puretext.us' target='blank'>Puretext</a>
                        </Card.Content>
                      </Card>
                    </Grid.Column>

                  </Grid.Row>
                  {/* <Grid.Row>
                    chart
                  </Grid.Row> */}
                  <Grid.Row columns={2} stretched >
                    <Grid.Column mobile={16} tablet={16} computer={8}>
                        <Credit auth={auth} credits={this.props.credits} />
                    </Grid.Column>
                    <Grid.Column mobile={16} tablet={16} computer={8}>
                        <Text auth={auth} texts={this.props.texts} />
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
            </Segment>
        );
    }
}

const mapStateToProps = state => {
    return {
        texts: state.text.texts,
        total: state.text.total,
        credits: state.credit,
        auth: state.auth
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchCredit: (items) => dispatch(actions.fetchCredit(items)),
        onFetchText: (items) => dispatch(actions.fetchText(items)),
        onFetchTextTotal: () => dispatch(actions.textTotal())
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);