import React, { Component } from 'react';
import StripeCheckout from 'react-stripe-checkout';
import { connect} from 'react-redux';
import * as actions from '../../store/actions';
import {Button, Icon} from 'semantic-ui-react';

class Payments extends Component {
    render() {
        return (
            <StripeCheckout
                name="Text Crunch"
                description="$5 for 5 text credits"
                amount={500}
                token={token => this.props.handleToken(token)}
                stripeKey={process.env.REACT_APP_STRIPE_KEY}
            >
            <Button icon='add' content='ADD CREDITS' color='black'/>
            {/* <Button><AddIcon />ADD CREDITS</Button> */}
            </StripeCheckout>
        );
    }
}

export default connect(null, actions)(Payments);