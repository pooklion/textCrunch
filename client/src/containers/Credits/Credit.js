import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../store/actions';
import { Grid, Segment } from 'semantic-ui-react';
import Payments from './payment';
import CreditHistory from '../../components/Credit/CreditActivity';

class Credits extends Component {
    componentDidMount() {
        console.log("Did Mount")
        this.props.onFetchCredit({limit:10});
    }
    componentDidUpdate(prevProps) {
        if (this.props.auth.credits !== prevProps.auth.credits) {
            console.log("Did Update")
            this.props.onFetchCredit({limit:10});
        }   
    }

    render() {
        const {auth,credits} = this.props
        return (
            <Segment style={{ padding: '8em 0em' }} vertical>
            <Grid container columns='equal' verticalAlign='top'>
                <Grid.Row stretched>
                <Grid.Column mobile={16} tablet={16} computer={8} style={{ padding: '0em 0em 8em 2em' }}>
                    <CreditHistory auth={auth} credits={credits}/>
                </Grid.Column>
                <Grid.Column mobile={16} tablet={16} computer={8} style={{ padding: '0em 0em 0em 2em' }}>
                    <p className='balance'>Credit Balance</p>
                    <div className='balance'>{auth && auth.credits}</div>
                    <Payments/>
                </Grid.Column>
                </Grid.Row>
            </Grid>
        </Segment>
        );
    }
}

const mapStateToProps = state => {
    return {
        credits: state.credit,
        auth: state.auth
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchCredit: (items) => dispatch(actions.fetchCredit(items))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Credits);