const mongoose = require('mongoose');
const keys = require('../config/keys');
const stripe = require('stripe')(keys.stripeSecretKey);
const requireLogin = require('../middlewares/requireLogin');
const Credit = mongoose.model('credit');

module.exports = (app) => {
    app.post('/api/stripe', requireLogin, async (req, res) => {
        const charge = await stripe.charges.create({
            amount: 500,
            currency: 'usd',
            description: '$5 for 5 credits',
            source: req.body.id
        })

        const credit = new Credit({
            dollar: '5',
            dateRefill: Date.now(),
            _user: req.user.id
        })
       
        try {
            console.log('billingroute');
            await credit.save();
            req.user.credits += 5;
            const user = await req.user.save();
            res.send(user);
        } catch (err) {
            console.log('error billingroute')
            res.status(422).send(err);
        }
    });

    app.get('/api/refill_activity', requireLogin, async (req, res) => {
        const limit = req.query.limit
        const refill_activity = await Credit.find({ _user: req.user.id }).sort({'dateRefill': -1}).limit(parseInt(limit));
        res.send(refill_activity);
    })
};